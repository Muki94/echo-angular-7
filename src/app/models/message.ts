import { Comment } from "./comment";

export class Message {
  id: number;
  userId: number;
  content: string;
  destination: string;
  rating: {};
  comments: Comment[];
  createdAt: Date;
  updatedAt: Date;
}
