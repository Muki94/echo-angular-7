export class Bug {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}
