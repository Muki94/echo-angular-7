export class Comment {
  id: number;
  userId: number;
  messageId: number;
  content: string;
  destination: string;
  createdAt: Date;
  updatedAt: Date;
}
