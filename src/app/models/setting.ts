export class Setting {
  id: number;
  name: string;
  createdAt: Date;
  updatedAt: Date;
}
