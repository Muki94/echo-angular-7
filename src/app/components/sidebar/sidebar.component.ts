import {
  Component,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  HostListener
} from "@angular/core";
import { shallowEqual } from "@angular/router/src/utils/collection";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

import { Bug } from "../../models/bug";

import { BugService } from "../../services/bug.service";
import { SidebarService } from "../../services/sidebar.service";

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
  @ViewChild("reportBugModal")
  reportBugModal: ElementRef;
  @ViewChild("navbar")
  navbar: ElementRef;

  scrollInit: any;
  scrollNext: any;

  bugForm: FormGroup;
  submitted = false;
  success = false;

  constructor(
    private bugService: BugService,
    public sidebar: SidebarService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.bugForm = this.formBuilder.group({
      message: ["", Validators.required]
    });
  }

  ngOnInit() {
    this.scrollInit = window.pageYOffset;
    this.sidebar.show();
  }

  openModal() {
    this.reportBugModal.nativeElement.style.display = "block";
  }

  closeModal() {
    this.reportBugModal.nativeElement.style.display = "none";
  }

  @HostListener("window:scroll", [])
  onScroll() {
    if (this.sidebar.visible) {
      this.scrollNext = window.pageYOffset;

      //add media rule to not apply functions that follow to devices above mobile
      const mediaRule = window.matchMedia("(max-width: 479px)");

      this.navbar.nativeElement.style.bottom =
        this.scrollNext > this.scrollInit && mediaRule.matches ? "-50px" : "0";

      this.scrollInit = this.scrollNext;
    }
  }

  @HostListener("window:resize", [])
  onWindowResize() {
    if (this.sidebar.visible) {
      //add media rule to not apply functions that follow to devices above mobile
      const mediaRule = window.matchMedia("(max-width: 479px)");
      if (!mediaRule.matches) this.navbar.nativeElement.style.bottom = "0";
    }
  }

  onSubmit() {
    this.submitted = true;

    if (this.bugForm.invalid) {
      return;
    }

    let bug = new Bug();
    bug.name = this.bugForm.controls.message.value;

    this.addBug(bug);

    this.success = true;
  }

  addBug(bug: Bug) {
    this.bugService.AddNewBug(bug).subscribe(result => {
      // if error
      this.closeModal();
    });
  }

  logout() {
    localStorage.clear();
    this.router.navigate(["/"]);
  }
}
