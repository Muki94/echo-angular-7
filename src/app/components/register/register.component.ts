import { Component, OnInit } from '@angular/core';

import { SidebarService } from "src/app/services/sidebar.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(public sidebar: SidebarService) { }

  ngOnInit() {
    this.sidebar.hide();
  }

}
