import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener
} from "@angular/core";
import { Message } from "@angular/compiler/src/i18n/i18n_ast";
import { Router } from "@angular/router";

import { MessageService } from "src/app/services/message.service";
import { SidebarService } from "src/app/services/sidebar.service";

@Component({
  selector: "app-main",
  templateUrl: "./main.component.html",
  styleUrls: ["./main.component.css"]
})
export class MainComponent implements OnInit {
  @ViewChild("addPostModal")
  addNewPostModal: ElementRef;

  @ViewChild("filterBox")
  filterBox: ElementRef;

  messages: Message[];

  scrollInit: any;
  scrollNext: any;

  searchFilter: any;

  constructor(private messageService: MessageService, private router: Router, public sidebar: SidebarService) { }

  ngOnInit() {
    this.scrollInit = window.pageYOffset;

    this.messageService.GetAllMessages(null).subscribe(messages => {
      this.messages = messages;
    });

    this.sidebar.show();
  }

  openModal() {
    this.addNewPostModal.nativeElement.style.display = "block";
  }

  closeModal() {
    this.addNewPostModal.nativeElement.style.display = "none";
  }

  @HostListener("window:scroll", [])
  onScroll() {
    this.scrollNext = window.pageYOffset;

    //add media rule to not apply functions that follow to devices above mobile
    let mediaRule = window.matchMedia("(max-width: 479px)");

    this.filterBox.nativeElement.style.top =
      this.scrollNext > this.scrollInit && mediaRule.matches ? "-50px" : "0";

    this.scrollInit = this.scrollNext;
  }

  @HostListener("window:resize", [])
  onWindowResize() {
    //add media rule to not apply functions that follow to devices above mobile
    let mediaRule = window.matchMedia("(max-width: 479px)");
    if (!mediaRule.matches) this.filterBox.nativeElement.style.top = "0";
  }

  search() {
    this.messageService
      .GetAllMessages(this.searchFilter)
      .subscribe(messages => {
        this.messages = messages;
      });
  }
}
