import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { User } from "./../models/user";
import { JwtHelperService } from "@auth0/angular-jwt";
import { Observable } from "rxjs";
import { JwtToken } from "../models/jwt-token";

@Injectable({
  providedIn: "root"
})
export class UserService {
  url = "https://echooo.herokuapp.com/";
  helper = new JwtHelperService();

  constructor(private http: HttpClient) {
    console.log("User service started...");
  }

  //get all
  GetAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.url + "users", JwtToken.getHeaders());
  }

  //get single
  GetSingleUser(id): Observable<User> {
    return this.http.get<User>(this.url + "users/" + id, JwtToken.getHeaders());
  }

  //register
  RegisterUser(user: User): Observable<JwtToken> {
    return this.http.post<JwtToken>(this.url + "users", user, JwtToken.getHeaders());
  }

  //authenticate
  AuthenticateUser(user: User): Observable<JwtToken> {
    return this.http.post<JwtToken>(this.url + "users/authenticate", user, JwtToken.getHeaders());
  }

  //is token valid
  isAuthenticated(): boolean {
    const token = localStorage.getItem("id_token");

    if (token != null) {
      return (this.helper.getTokenExpirationDate(token) == null)
        ? true
        : !this.helper.isTokenExpired(token);
    }

    return false;
  }

  //update
  //delete
}
