import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";

import { UserService } from "../services/user.service";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private auth: UserService) { }

  canActivate(): boolean {
    if (this.auth != null && !this.auth.isAuthenticated()) {
      this.router.navigate(["/login"]);
      return false;
    }

    return true;
  }
}
