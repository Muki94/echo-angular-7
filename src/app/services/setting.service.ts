import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { JwtToken } from '../models/jwt-token';

@Injectable({
  providedIn: "root"
})
export class SettingService {
  url = "https://echooo.herokuapp.com/";

  constructor(private http: HttpClient) {
    console.log("Setting service started...");
  }

  //get all
  GetAllSettings() {
    return this.http.get(this.url + "settings", JwtToken.getHeaders());
  }
  //get single
  GetSingleSetting(id) {
    return this.http.get(this.url + "settings/" + id, JwtToken.getHeaders());
  }

  //add
  //update
  //delete
}
